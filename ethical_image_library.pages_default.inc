<?php
/**
 * @file
 * ethical_image_library.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_image_library_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_image_library';
  $page->task = 'page';
  $page->admin_title = 'Image library';
  $page->admin_description = '';
  $page->path = 'photos';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Photos',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_image_library_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_image_library';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '58c953e0-fd5e-4efe-9497-09f7a041635a';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7f50c9ae-c503-43d1-8b4e-8e9bc4238332';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openethical_image_library-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'teaser',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7f50c9ae-c503-43d1-8b4e-8e9bc4238332';
  $display->content['new-7f50c9ae-c503-43d1-8b4e-8e9bc4238332'] = $pane;
  $display->panels['contentmain'][0] = 'new-7f50c9ae-c503-43d1-8b4e-8e9bc4238332';
  $pane = new stdClass();
  $pane->pid = 'new-0e2e425e-00c2-4356-bc60-ada26293e9f3';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-BG3lx017lrrb1dKAVCZRwUAX6jHt1zf3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Project',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0e2e425e-00c2-4356-bc60-ada26293e9f3';
  $display->content['new-0e2e425e-00c2-4356-bc60-ada26293e9f3'] = $pane;
  $display->panels['sidebar'][0] = 'new-0e2e425e-00c2-4356-bc60-ada26293e9f3';
  $pane = new stdClass();
  $pane->pid = 'new-2e05f4c2-eeda-4e95-90e6-828e7a2ccc75';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-Ftxeu4aJdmY2bguvejSHt5iQ1KK03x4i';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '2e05f4c2-eeda-4e95-90e6-828e7a2ccc75';
  $display->content['new-2e05f4c2-eeda-4e95-90e6-828e7a2ccc75'] = $pane;
  $display->panels['sidebar'][1] = 'new-2e05f4c2-eeda-4e95-90e6-828e7a2ccc75';
  $pane = new stdClass();
  $pane->pid = 'new-8631e01a-ebb7-46e9-b2bf-b3513c8451eb';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-yFHt6Kn1DL77Ds0wiRDj1unYI1P1VrUi';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '8631e01a-ebb7-46e9-b2bf-b3513c8451eb';
  $display->content['new-8631e01a-ebb7-46e9-b2bf-b3513c8451eb'] = $pane;
  $display->panels['sidebar'][2] = 'new-8631e01a-ebb7-46e9-b2bf-b3513c8451eb';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_image_library'] = $page;

  return $pages;

}
